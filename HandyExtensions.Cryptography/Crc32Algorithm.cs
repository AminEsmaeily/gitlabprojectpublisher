﻿using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace HandyExtensions.Extensions.Cryptography
{
    internal class Crc32Algorithm : HashAlgorithm
    {
        public const uint DefaultPolynomial = 0xedb88320;
        public const uint DefaultSeed = 0xffffffff;
        private static uint[] defaultTable;

        private uint hash;
        private readonly uint seed;
        private readonly uint[] table;

        internal Crc32Algorithm()
        {
            table = InitializeTable(DefaultPolynomial);
            seed = DefaultSeed;
            Initialize();
        }

        internal Crc32Algorithm(uint polynomial, uint seed)
        {
            table = InitializeTable(polynomial);
            this.seed = seed;
            Initialize();
        }

        public override int HashSize
        {
            get { return 32; }
        }

        public override void Initialize()
        {
            hash = seed;
        }

        protected override void HashCore(byte[] buffer, int start, int length)
        {
            hash = CalculateHash(table, hash, buffer, start, length);
        }

        protected override byte[] HashFinal()
        {
            var hashBuffer = UInt32ToBigEndianBytes(~hash);
            HashValue = hashBuffer;
            return hashBuffer;
        }

        internal static uint Compute(byte[] buffer)
        {
            return ~CalculateHash(InitializeTable(DefaultPolynomial), DefaultSeed, buffer, 0, buffer.Length);
        }

        internal static uint Compute(uint seed, byte[] buffer)
        {
            return ~CalculateHash(InitializeTable(DefaultPolynomial), seed, buffer, 0, buffer.Length);
        }

        internal static uint Compute(uint polynomial, uint seed, byte[] buffer)
        {
            return ~CalculateHash(InitializeTable(polynomial), seed, buffer, 0, buffer.Length);
        }

        private static uint[] InitializeTable(uint polynomial)
        {
            if (polynomial == DefaultPolynomial && defaultTable != null)
                return defaultTable;

            var createTable = new uint[256];
            for (var i = 0; i < 256; i++)
            {
                var entry = (uint) i;
                for (var j = 0; j < 8; j++)
                    if ((entry & 1) == 1)
                        entry = (entry >> 1) ^ polynomial;
                    else
                        entry = entry >> 1;
                createTable[i] = entry;
            }

            if (polynomial == DefaultPolynomial)
                defaultTable = createTable;

            return createTable;
        }

        private static uint CalculateHash(uint[] table, uint seed, byte[] buffer, int start, int size)
        {
            var crc = seed;
            for (var i = start; i < size; i++)
                unchecked
                {
                    crc = (crc >> 8) ^ table[buffer[i] ^ crc & 0xff];
                }
            return crc;
        }

        private byte[] UInt32ToBigEndianBytes(uint x)
        {
            return new[]
            {
                (byte) ((x >> 24) & 0xff),
                (byte) ((x >> 16) & 0xff),
                (byte) ((x >> 8) & 0xff),
                (byte) (x & 0xff)
            };
        }

        internal static string GetCrc32(byte[] bytes)
        {
            var crc32 = new Crc32Algorithm();
            var hash = string.Empty;
            foreach (var b in crc32.ComputeHash(bytes)) hash += b.ToString("x2");
            return hash;
        }

        internal static string GetCrc32(string s)
        {
            var bytes = Encoding.ASCII.GetBytes(s);
            var crc32 = new Crc32Algorithm();
            var hash = string.Empty;
            foreach (var b in crc32.ComputeHash(bytes)) hash += b.ToString("x2");
            return hash;
        }

        internal static string GetCrc32(FileStream fs)
        {
            var crc32 = new Crc32Algorithm();
            var hash = string.Empty;
            foreach (var b in crc32.ComputeHash(fs)) hash += b.ToString("x2");
            return hash;
        }
    }
}