﻿using System;
using System.IO;
using HandyExtensions.Extensions.Cryptography;

public static class CryptoExt
{
    public static string EncryptToRijndael(this string str, string key)
    {
        if (string.IsNullOrEmpty(str)) throw new ArgumentException("Argument is null or empty", nameof(str));
        if (string.IsNullOrEmpty(key)) throw new ArgumentException("Argument is null or empty", nameof(key));
        return RijndaelCrypto.Encrypt(str, key);
    }

    public static string EncryptToTripleDes(this string str, string key)
    {
        if (string.IsNullOrEmpty(str)) throw new ArgumentException("Argument is null or empty", nameof(str));
        if (string.IsNullOrEmpty(key)) throw new ArgumentException("Argument is null or empty", nameof(key));
        return TripleDesCrypto.Encrypt(str, key);
    }

    public static string DecryptFromRijndael(this string str, string key)
    {
        if (string.IsNullOrEmpty(str)) throw new ArgumentException("Argument is null or empty", nameof(str));
        if (string.IsNullOrEmpty(key)) throw new ArgumentException("Argument is null or empty", nameof(key));
        return RijndaelCrypto.Decrypt(str, key);
    }

    public static string DecryptFromTripleDes(this string str, string key)
    {
        if (string.IsNullOrEmpty(str)) throw new ArgumentException("Argument is null or empty", nameof(str));
        if (string.IsNullOrEmpty(key)) throw new ArgumentException("Argument is null or empty", nameof(key));
        return TripleDesCrypto.Decrypt(str, key);
    }

    public static byte[] EncryptToRijndael(this byte[] bytes, string key)
    {
        if (bytes == null) throw new ArgumentNullException(nameof(bytes));
        if (string.IsNullOrEmpty(key)) throw new ArgumentException("Argument is null or empty", nameof(key));
        return RijndaelCrypto.Encrypt(bytes, key);
    }

    public static byte[] EncryptToTripleDes(this byte[] bytes, string key)
    {
        if (bytes == null) throw new ArgumentNullException(nameof(bytes));
        if (string.IsNullOrEmpty(key)) throw new ArgumentException("Argument is null or empty", nameof(key));
        return TripleDesCrypto.Encrypt(bytes, key);
    }

    public static byte[] DecryptFromRijndael(this byte[] bytes, string key)
    {
        if (bytes == null) throw new ArgumentNullException(nameof(bytes));
        if (string.IsNullOrEmpty(key)) throw new ArgumentException("Argument is null or empty", nameof(key));
        return RijndaelCrypto.Decrypt(bytes, key);
    }

    public static byte[] DecryptFromTripleDes(this byte[] bytes, string key)
    {
        if (bytes == null) throw new ArgumentNullException(nameof(bytes));
        if (string.IsNullOrEmpty(key)) throw new ArgumentException("Argument is null or empty", nameof(key));
        return TripleDesCrypto.Decrypt(bytes, key);
    }

    public static void EncryptToRijndael(this string inputFile, string outputFile, string key)
    {
        if (string.IsNullOrEmpty(key)) throw new ArgumentException("Argument is null or empty", nameof(key));
        if (string.IsNullOrEmpty(outputFile))
            throw new ArgumentException("Argument is null or empty", nameof(outputFile));
        if (string.IsNullOrEmpty(inputFile))
            throw new ArgumentException("Argument is null or empty", nameof(inputFile));
        RijndaelCrypto.Encrypt(inputFile, outputFile, key);
    }

    public static void EncryptToTripleDes(this string inputFile, string outputFile, string key)
    {
        if (string.IsNullOrEmpty(key)) throw new ArgumentException("Argument is null or empty", nameof(key));
        if (string.IsNullOrEmpty(outputFile))
            throw new ArgumentException("Argument is null or empty", nameof(outputFile));
        if (string.IsNullOrEmpty(inputFile))
            throw new ArgumentException("Argument is null or empty", nameof(inputFile));
        TripleDesCrypto.Encrypt(inputFile, outputFile, key);
    }

    public static void DecryptFromRijndael(this string inputFile, string outputFile, string key)
    {
        if (string.IsNullOrEmpty(key)) throw new ArgumentException("Argument is null or empty", nameof(key));
        if (string.IsNullOrEmpty(outputFile))
            throw new ArgumentException("Argument is null or empty", nameof(outputFile));
        if (string.IsNullOrEmpty(inputFile))
            throw new ArgumentException("Argument is null or empty", nameof(inputFile));
        RijndaelCrypto.Decrypt(inputFile, outputFile, key);
    }

    public static void DecryptFromTripleDes(this string inputFile, string outputFile, string key)
    {
        if (string.IsNullOrEmpty(inputFile))
            throw new ArgumentException("Argument is null or empty", nameof(inputFile));
        if (string.IsNullOrEmpty(outputFile))
            throw new ArgumentException("Argument is null or empty", nameof(outputFile));
        if (string.IsNullOrEmpty(key)) throw new ArgumentException("Argument is null or empty", nameof(key));
        TripleDesCrypto.Decrypt(inputFile, outputFile, key);
    }


    public static string GetMd5Hash(this string str)
    {
        if (string.IsNullOrEmpty(str)) throw new ArgumentException("Argument is null or empty", nameof(str));
        return Hash.ComputeMD5Checksum(str);
    }

    public static string GetMd5Hash(this byte[] bytes)
    {
        if (bytes == null) throw new ArgumentNullException(nameof(bytes));
        return Hash.ComputeMD5Checksum(bytes);
    }

    public static string GetMd5Hash(this Stream stream)
    {
        if (stream == null) throw new ArgumentNullException(nameof(stream));
        return Hash.ComputeMD5Checksum(stream);
    }

    public static string GetCrc32Hash(this string str)
    {
        if (string.IsNullOrEmpty(str)) throw new ArgumentException("Argument is null or empty", nameof(str));
        return Hash.ComputeCRC32Checksum(str);
    }

    public static string GetCrc32Hash(this byte[] bytes)
    {
        if (bytes == null) throw new ArgumentNullException(nameof(bytes));
        return Hash.ComputeCRC32Checksum(bytes);
    }

    public static string GetCrc32Hash(this Stream stream)
    {
        if (stream == null) throw new ArgumentNullException(nameof(stream));
        return Hash.ComputeCRC32Checksum(stream);
    }

    public static string GetSha1Hash(this string str)
    {
        if (string.IsNullOrEmpty(str)) throw new ArgumentException("Argument is null or empty", nameof(str));
        return Hash.ComputeSHA1Checksum(str);
    }

    public static string GetSha1Hash(this byte[] bytes)
    {
        if (bytes == null) throw new ArgumentNullException(nameof(bytes));
        return Hash.ComputeSHA1Checksum(bytes);
    }

    public static string GetSha1Hash(this Stream stream)
    {
        if (stream == null) throw new ArgumentNullException(nameof(stream));
        return Hash.ComputeSHA1Checksum(stream);
    }

    public static string GetSha256Hash(this string str)
    {
        if (string.IsNullOrEmpty(str)) throw new ArgumentException("Argument is null or empty", nameof(str));
        return Hash.ComputeSHA256Checksum(str);
    }

    public static string GetSha256Hash(this byte[] bytes)
    {
        if (bytes == null) throw new ArgumentNullException(nameof(bytes));
        return Hash.ComputeSHA256Checksum(bytes);
    }

    public static string GetSha256Hash(this Stream stream)
    {
        if (stream == null) throw new ArgumentNullException(nameof(stream));
        return Hash.ComputeSHA256Checksum(stream);
    }

    public static string GetSha384Hash(this string str)
    {
        if (string.IsNullOrEmpty(str)) throw new ArgumentException("Argument is null or empty", nameof(str));
        return Hash.ComputeSHA384Checksum(str);
    }

    public static string GetSha384Hash(this byte[] bytes)
    {
        if (bytes == null) throw new ArgumentNullException(nameof(bytes));
        return Hash.ComputeSHA384Checksum(bytes);
    }

    public static string GetSha384Hash(this Stream stream)
    {
        if (stream == null) throw new ArgumentNullException(nameof(stream));
        return Hash.ComputeSHA384Checksum(stream);
    }

    public static string GetSha512Hash(this string str)
    {
        if (string.IsNullOrEmpty(str)) throw new ArgumentException("Argument is null or empty", nameof(str));
        return Hash.ComputeSHA512Checksum(str);
    }

    public static string GetSha512Hash(this byte[] bytes)
    {
        if (bytes == null) throw new ArgumentNullException(nameof(bytes));
        return Hash.ComputeSHA512Checksum(bytes);
    }

    public static string GetSha512Hash(this Stream stream)
    {
        if (stream == null) throw new ArgumentNullException(nameof(stream));
        return Hash.ComputeSHA512Checksum(stream);
    }
}