﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GitlabProjectPublisherWebHook
{
    public class WebServer
    {
        private readonly HttpListener _listener = new HttpListener();
        private readonly Action<HttpListenerRequest> _responderMethod;

        public WebServer(IReadOnlyCollection<string> prefixes, Action<HttpListenerRequest> method)
        {
            if (!HttpListener.IsSupported)
            {
                throw new NotSupportedException("Needs Windows XP SP2, Server 2003 or later.");
            }
            
            if (prefixes == null || prefixes.Count == 0)
            {
                throw new ArgumentException("URI prefixes are required");
            }

            foreach (var s in prefixes)
            {
                _listener.Prefixes.Add(s);
            }

            _responderMethod = method ?? throw new ArgumentException("responder method required");
            _listener.Start();
        }

        public WebServer(Action<HttpListenerRequest> method, params string[] prefixes)
           : this(prefixes, method)
        {
        }

        public void Run()
        {
            ThreadPool.QueueUserWorkItem(async o =>
            {
                Console.WriteLine("Webserver running...");
                await Task.Run(() =>
                {
                    try
                    {
                        while (_listener.IsListening)
                        {
                            ThreadPool.QueueUserWorkItem(async c =>
                            {
                                var ctx = c as HttpListenerContext;
                                try
                                {
                                    if (ctx == null)
                                    {
                                        return;
                                    }

                                    _responderMethod(ctx.Request);
                                    ctx.Response.StatusCode = (int)HttpStatusCode.OK;
                                }
                                catch(Exception ex)
                                {
                                    Console.WriteLine("Error in calling response method.\n" + ex.Message);
                                }
                                finally
                                {
                                    // always close the stream
                                    if (ctx != null)
                                    {
                                        ctx.Response.OutputStream.Close();
                                    }
                                }
                            }, _listener.GetContext());
                        }

                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Error in listening to the network.\n" + ex.Message);
                    }
                });
            });
        }

        public void Stop()
        {
            _listener.Stop();
            _listener.Close();
        }
    }
}
