﻿using System.ServiceProcess;

namespace GitlabProjectPublisher
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                new PushListener(args)
            };
            ServiceBase.Run(ServicesToRun);
        }
    }
}
