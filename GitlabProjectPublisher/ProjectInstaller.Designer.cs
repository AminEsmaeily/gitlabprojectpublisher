﻿namespace GitlabProjectPublisher
{
    partial class ProjectInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pushListenerProcessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
            this.pushListenerInstaller = new System.ServiceProcess.ServiceInstaller();
            // 
            // pushListenerProcessInstaller
            // 
            this.pushListenerProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
            this.pushListenerProcessInstaller.Password = null;
            this.pushListenerProcessInstaller.Username = null;
            // 
            // pushListenerInstaller
            // 
            this.pushListenerInstaller.Description = "By using this service, we can listen to Git repository to pushes then pull them a" +
    "nd deploy on local computer or IIS";
            this.pushListenerInstaller.DisplayName = "Gitlab Project Publisher";
            this.pushListenerInstaller.ServiceName = "Gitlab Push Listener";
            // 
            // ProjectInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.pushListenerProcessInstaller,
            this.pushListenerInstaller});

        }

        #endregion

        private System.ServiceProcess.ServiceProcessInstaller pushListenerProcessInstaller;
        private System.ServiceProcess.ServiceInstaller pushListenerInstaller;
    }
}