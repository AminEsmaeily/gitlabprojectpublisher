﻿using GitlabProjectPublisherWebHook;
using LibGit2Sharp;
using LibGit2Sharp.Handlers;
using SimpleLogger.Logging.Handlers;
using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.ServiceProcess;
using System.Threading.Tasks;
using static SimpleLogger.Logger;

namespace GitlabProjectPublisher
{
    public partial class PushListener : ServiceBase
    {
        private readonly string _key = 6555448.ToString("X2") + (0x89550 << 8) + 0x25 + 898854112.ToString("X") + (0x98 >> 5) + (13671604.ToString("X")) + (20054455223654.ToString("X"));
        private readonly string _defaultListeningUrl = "http://localhost";
        private readonly string _defaultListeningEndPoint = "gitlistener";
        private readonly ushort _defaultListeningPort = 15150;

        public string Username
        {
            get => Properties.Settings.Default.Username;
            set
            {
                Properties.Settings.Default.Username = value;
                Properties.Settings.Default.Save();
            }
        }

        public string Password
        {
            get => Properties.Settings.Default.Password.DecryptFromRijndael(_key);
            set
            {
                Properties.Settings.Default.Password = value.EncryptToRijndael(_key);
                Properties.Settings.Default.Save();
            }
        }

        public string SourcePath
        {
            get => Properties.Settings.Default.SourcePath;
            set
            {
                Properties.Settings.Default.SourcePath = value;
                Properties.Settings.Default.Save();
            }
        }

        public string ListeningUrl
        {
            get => Properties.Settings.Default.ListeningUrl;
            set
            {
                Properties.Settings.Default.ListeningUrl = value;
                Properties.Settings.Default.Save();
            }
        }

        public ushort ListeningPort
        {
            get => Properties.Settings.Default.ListeningPort;
            set
            {
                Properties.Settings.Default.ListeningPort = value;
                Properties.Settings.Default.Save();
            }
        }

        public string ListeningEndPoint
        {
            get => Properties.Settings.Default.ListeningEndPoint;
            set
            {
                Properties.Settings.Default.ListeningEndPoint = value;
                Properties.Settings.Default.Save();
            }
        }

        public string PathToDeploy
        {
            get => Properties.Settings.Default.PathToDeploy;
            set
            {
                Properties.Settings.Default.PathToDeploy = value;
                Properties.Settings.Default.Save();
            }
        }

        public enum ServiceState
        {
            SERVICE_STOPPED = 0x00000001,
            SERVICE_START_PENDING = 0x00000002,
            SERVICE_STOP_PENDING = 0x00000003,
            SERVICE_RUNNING = 0x00000004,
            SERVICE_CONTINUE_PENDING = 0x00000005,
            SERVICE_PAUSE_PENDING = 0x00000006,
            SERVICE_PAUSED = 0x00000007,
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct ServiceStatus
        {
            public int dwServiceType;
            public ServiceState dwCurrentState;
            public int dwControlsAccepted;
            public int dwWin32ExitCode;
            public int dwServiceSpecificExitCode;
            public int dwCheckPoint;
            public int dwWaitHint;
        };

        [DllImport("advapi32.dll", SetLastError = true)]
        private static extern bool SetServiceStatus(System.IntPtr handle, ref ServiceStatus serviceStatus);

        public PushListener(string[] args)
        {
            InitializeComponent();

            LoggerHandlerManager
                .AddHandler(new FileLoggerHandler("GitlabPublishListener.log"));

            try
            {
                if (args.Length > 0)
                {
                    Log(Level.Info, "Reading configuration parameters from input argument.");
                    FillParameters(args);
                }
                else
                    Log(Level.Info, "Input argument is empty. Reading values from previously saved config.");
            }
            catch (Exception ex)
            {
                Log(Level.Error, "Error in initializing service.\n" + ex.Message);
            }
        }

        /// <summary>
        /// Gets required values from args parameter and sets them to local variables.
        /// </summary>
        /// <param name="args">
        ///     user:           Git username(mandatory)
        ///     pass:           Git password(mandatory)
        ///     sourcepath:     Local directory of the repository(mandatory)
        ///     path2deploy     Specifies the directory that will be used for deployment
        ///     url:            Listener address(optional), default = "http://localhost"
        ///     port:           Port number of the listening URL(optional), default = 15150
        ///     endpoint:       Listener endpoint(optional), default = "gitlistener"
        /// </param>
        private void FillParameters(string[] args)
        {
            try
            {
                var username = GetArgValue(args, "user");
                if (!string.IsNullOrEmpty(username))
                    Log($"User name is {username}");
                else
                    throw new Exception("Parameter not found (Username => user).");

                var password = GetArgValue(args, "pass");
                if (!string.IsNullOrEmpty(password))
                    Log($"Password is **********");
                else
                    throw new Exception("Parameter not found (Password => pass).");

                var sourcePath = GetArgValue(args, "sourcepath");
                if (!string.IsNullOrEmpty(sourcePath))
                    Log($"Git source path is {sourcePath}");
                else
                    throw new Exception("Parameter not found (Git source => sourcepath).");

                var pathToDeploy = GetArgValue(args, "path2deploy");
                if (!string.IsNullOrEmpty(pathToDeploy))
                    Log($"Deploy path is {pathToDeploy}");
                else
                    throw new Exception("Parameter not found (Path to deploy => path2deploy)");

                var url = GetArgValue(args, "url");
                var endpoint = GetArgValue(args, "endpoint");
                var port = GetArgValue(args, "port");

                Username = username;
                Password = password;
                SourcePath = sourcePath;
                PathToDeploy = pathToDeploy;

                if (!string.IsNullOrEmpty(url))
                    ListeningUrl = url;
                else
                    ListeningUrl = _defaultListeningUrl;

                if (!string.IsNullOrEmpty(endpoint))
                    ListeningEndPoint = endpoint;
                else
                    ListeningEndPoint = _defaultListeningEndPoint;

                if (!string.IsNullOrEmpty(port))
                {
                    if (!ushort.TryParse(port, out var localPort))
                        throw new Exception("Port number is invalid. Port number should be between 0 and 65535");

                    ListeningPort = localPort;
                }
                else
                    ListeningPort = _defaultListeningPort;
            }
            catch (Exception ex)
            {
                Log(Level.Error, "Error in reading parameters. Error = " + ex.Message);
                throw new Exception("Error in reading parameters.", ex);
            }
        }

        private string GetArgValue(string[] args, string param)
        {
            var argument = args.FirstOrDefault(f => f.ToLower().StartsWith(param.ToLower() + "="));
            if (argument == null)
                return null;

            return argument.Substring(argument.IndexOf("=") + 1);
        }

        protected override void OnStart(string[] args)
        {
            Log("Starting Push Listener service...");
            ServiceStatus serviceStatus = new ServiceStatus
            {
                dwCurrentState = ServiceState.SERVICE_START_PENDING,
                dwWaitHint = 5000
            };
            SetServiceStatus(ServiceHandle, ref serviceStatus);

            if (args.Length > 0)
            {
                Log(Level.Info, "Reading configuration parameters from input argument.");
                FillParameters(args);
            }
            else
            {
                Log(Level.Info, "Input argument is empty. Reading values from previously saved config.");
                if (string.IsNullOrEmpty(Username) ||
                    string.IsNullOrEmpty(Password) ||
                    string.IsNullOrEmpty(SourcePath))
                    throw new Exception("Config values not found.");
            }

            UriBuilder uriBuilder = new UriBuilder(ListeningUrl)
            {
                Port = ListeningPort,
                Path = ListeningEndPoint.EndsWith("/") ? ListeningEndPoint : ListeningEndPoint + "/"
            };
            Log("Listening Url = \"" + uriBuilder.Uri + "\"");

            new WebServer(RequestReceived, uriBuilder.Uri.ToString()).Run();

            Log("Listener service started successfully.");
            serviceStatus.dwCurrentState = ServiceState.SERVICE_RUNNING;
            SetServiceStatus(ServiceHandle, ref serviceStatus);
        }

        public void RequestReceived(HttpListenerRequest request)
        {
            var X_Gitlab_Event = request.Headers["X-Gitlab-Event"];
            if (!X_Gitlab_Event.Equals("Tag Push Hook"))
            {
                Log(X_Gitlab_Event + " detected. Do nothing");
                return;
            }

            Task.Run(() =>
            {
                Log(Level.Info, " ");
                Log(Level.None, "================================================");
                try
                {
                    Log(Level.Warning, "Tag Push Hook detected. Starting to Pull...");
                    PullRepo();
                    Log("Repo pulled successfully.");
                }
                catch (Exception ex)
                {
                    Log(Level.Error, "Error in pulling repository.\n" + ex.Message);
                    return;
                }

                try
                {
                    Log("Publishing the solution...");
                    PublishSolution();
                }
                catch (Exception ex)
                {
                    Log(Level.Error, "Error in publishing the solution.\n" + ex.Message);
                    return;
                }
            });
        }

        public void PullRepo()
        {
            try
            {
                using (var repo = new Repository(SourcePath))
                {
                    // Credential information to fetch
                    PullOptions options = new PullOptions
                    {
                        FetchOptions = new FetchOptions()
                    };
                    options.FetchOptions.CredentialsProvider = new CredentialsHandler(
                        (url, usernameFromUrl, types) =>
                            new UsernamePasswordCredentials()
                            {
                                Username = Username,
                                Password = Password
                            });

                    // User information to create a merge commit
                    var signature = new Signature(
                        new Identity("MERGE_USER_NAME", "MERGE_USER_EMAIL"), DateTimeOffset.Now);

                    // Pull
                    Commands.Pull(repo, signature, options);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Exception in pulling repository.", ex);
            }
        }

        public void PublishSolution()
        {
            var startInfo = new ProcessStartInfo
            {
                WorkingDirectory = Path.Combine(SourcePath, "AdaIt.Automation.Web"),
                CreateNoWindow = false,
                FileName = "dotnet.exe",
                Arguments = $"publish -f netcoreapp2.0 -o \"{PathToDeploy}\" -c Release /property:PublishWithAspNetCoreTargetManifest=false --no-restore",
                RedirectStandardInput = true,
                RedirectStandardError = true,
                RedirectStandardOutput = true,
                UseShellExecute = false,
                ErrorDialog = false
            };

            var process = new Process
            {
                StartInfo = startInfo
            };
            process.EnableRaisingEvents = true;
            process.Exited += PublishProcessExited;
            process.Start();
        }

        private void PublishProcessExited(object sender, EventArgs e)
        {
            if (sender is Process p)
            {
                if (p.ExitCode == 0)
                    Log("Publish completed successfully.");
                else
                    Log("Publish failed. Exit Code = " + p.ExitCode);

                var message = p.StandardOutput.ReadToEnd();
                Log(message);
            }
            else
                Log("Solution publish finished.");
        }

        protected override void OnStop()
        {
            Log("Stopping Push Listener service...");
            ServiceStatus serviceStatus = new ServiceStatus
            {
                dwCurrentState = ServiceState.SERVICE_STOP_PENDING,
                dwWaitHint = 5000
            };
            SetServiceStatus(ServiceHandle, ref serviceStatus);



            Log(Level.Warning, "Listener service stopped");
            serviceStatus.dwCurrentState = ServiceState.SERVICE_STOPPED;
            SetServiceStatus(ServiceHandle, ref serviceStatus);
        }

        protected override void OnContinue()
        {
            Log("Continuing the Push listener service...");
            ServiceStatus serviceStatus = new ServiceStatus
            {
                dwCurrentState = ServiceState.SERVICE_CONTINUE_PENDING,
                dwWaitHint = 5000
            };
            SetServiceStatus(ServiceHandle, ref serviceStatus);



            Log("Listener service continued running");
            serviceStatus.dwCurrentState = ServiceState.SERVICE_RUNNING;
            SetServiceStatus(ServiceHandle, ref serviceStatus);
        }

        protected override void OnPause()
        {
            Log("Stopping Push listener service...");
            ServiceStatus serviceStatus = new ServiceStatus
            {
                dwCurrentState = ServiceState.SERVICE_PAUSE_PENDING,
                dwWaitHint = 5000
            };
            SetServiceStatus(ServiceHandle, ref serviceStatus);



            Log("Push listener service paused.");
            serviceStatus.dwCurrentState = ServiceState.SERVICE_PAUSED;
            SetServiceStatus(ServiceHandle, ref serviceStatus);
        }
    }
}
