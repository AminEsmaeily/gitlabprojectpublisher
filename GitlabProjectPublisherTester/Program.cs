﻿using GitlabProjectPublisher;
using GitlabProjectPublisherWebHook;
using System.Net;
using System.Threading.Tasks;
using static System.Console;
using HandyExtensions.Extensions.Cryptography;

namespace GitlabProjectPublisherTester
{
    class Program
    {
        static WebServer ws;
        static void Main(string[] args)
        {
            var listeningUrl = "http://localhost:15150/gitlistener/";
            WriteLine("Listening to {0}", listeningUrl);

            var key = 6555448.ToString("X2") + (0x89550 << 8) + 0x25 + 898854112.ToString("X") + (0x98 >> 5) + (13671604.ToString("X")) + (20054455223654.ToString("X"));
            WriteLine(key);
            var pass = "5584255842";
            var encrypted = pass.EncryptToRijndael(key);
            WriteLine(encrypted);
            WriteLine(encrypted.DecryptFromRijndael(key));

            ws = new WebServer(RequestReceived, listeningUrl);
            //ws.Run();
            WriteLine("If you want to stop it, Press any key to quit.");

            ReadKey();
            ws.Stop();
            WriteLine("Listener stopped.");
        }

        public static void RequestReceived(HttpListenerRequest request)
        {
            var X_Gitlab_Event = request.Headers["X-Gitlab-Event"];
            if (!X_Gitlab_Event.Equals("Tag Push Hook"))
            {
                WriteLine(X_Gitlab_Event + " detected. Do nothing");
                return;
            }

            Task.Run(() =>
            {
                var args = new string[]
                {
                    "user=aminesmaeily",
                    "pass=**********",
                    $"sourcepath={@"E:\Workspace\.Net Core\AdaIt\"}",
                    $"path2deploy={@"c:\inetpub\wwwroot\AdaIt"}",
                    "url=http://localhost",
                    "port=15150",
                    "endpoint=gitlistener"
                };
                var pushListener = new PushListener(args);
                WriteLine("Tag Push Hook detected. Starting to Pull...");
                pushListener.PullRepo();
                WriteLine("Repo pulled successfully.");

                WriteLine("Publishing the solution...");
                pushListener.PublishSolution();
            });
        }

        /*private static void PullRepo(string sourceLocalPath, string username, string password)
        {
            try
            {
                using (var repo = new Repository(sourceLocalPath))
                {
                    // Credential information to fetch
                    PullOptions options = new PullOptions
                    {
                        FetchOptions = new FetchOptions()
                    };
                    options.FetchOptions.CredentialsProvider = new CredentialsHandler(
                        (url, usernameFromUrl, types) =>
                            new UsernamePasswordCredentials()
                            {
                                Username = username,
                                Password = password
                            });

                    // User information to create a merge commit
                    var signature = new Signature(
                        new Identity("MERGE_USER_NAME", "MERGE_USER_EMAIL"), DateTimeOffset.Now);

                    // Pull
                    Commands.Pull(repo, signature, options);
                }
            }
            catch(Exception ex)
            {
                throw new Exception("Exception in pulling repository.", ex);
            }
        }

        private static void PublishSolution(string sourceLocalPath, string pathToDeploy)
        {
            try
            {
                var startInfo = new ProcessStartInfo
                {
                    WorkingDirectory = Path.Combine(sourceLocalPath, "AdaIt.Automation.Web"),
                    CreateNoWindow = false,
                    FileName = "dotnet.exe",
                    Arguments = $"publish -f netcoreapp2.0 -o \"{pathToDeploy}\" -c Release /property:PublishWithAspNetCoreTargetManifest=false",
                    RedirectStandardInput = true,
                    RedirectStandardError = true,
                    UseShellExecute = false,
                    ErrorDialog = false
                };

                var process = new Process
                {
                    StartInfo = startInfo
                };
                process.EnableRaisingEvents = true;
                process.Exited += PublishProcessExited;
                process.Start();
            }
            catch(Exception ex)
            {
                throw new Exception("Error in deploying the project", ex);
            }
        }

        private static void PublishProcessExited(object sender, EventArgs e)
        {
            if (sender is Process p)
            {
                WriteLine();
                WriteLine("Publish completed. Exit Code = {0}", p.ExitCode);
                WriteLine("=========================================");
            }
            else
                WriteLine("Solution publish finished.");
        }*/
    }
}
